// Реалізувати можливість зміни колірної теми користувача.Завдання має бути виконане на чистому
// Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Взяти будь-яке готове домашнє завдання з HTML/CSS.
// Додати на макеті кнопку "Змінити тему".
// При натисканні на кнопку - змінювати колірну гаму сайту(кольори кнопок, фону тощо) на ваш
// розсуд.При повторному натискання - повертати все як було спочатку - начебто для сторінки
// доступні дві колірні теми.
// Вибрана тема повинна зберігатися після перезавантаження сторінки

// Варіант 1

// const pushBtn = document.querySelector('.theme-button');
// const bodyBgr = document.querySelector('body');
// const logoColor = document.querySelector('.logo span');
// const sectionTextColor = document.querySelectorAll('.section-text');
// const mainMenuLinkColor = document.querySelectorAll('.main-menu-link');
// const footerMenuLinkColor = document.querySelectorAll('.footer-menu-link');
// let styleChanged = true;

// const LSStyleChanged = localStorage.getItem('styleChanged');
// if (LSStyleChanged !== null) {
//   styleChanged = LSStyleChanged === 'true';
// }

// function updateStyles() {
//   if (styleChanged) {bodyBgr.style.backgroundColor = '#292c31';
//         logoColor.style.color = '#ffffff';
//         sectionTextColor.forEach((text) => {
//             text.style.color = '#ffffff';
//         });
//         mainMenuLinkColor.forEach((link) => {
//             link.style.color = '#ffffff';
//             link.onmouseover = function (event) {
//                 event.target.style.color = '#35444f';
//             }
//             link.onmouseout = function (event) {
//                 event.target.style.color = '#ffffff';
//             }
//         });
//         footerMenuLinkColor.forEach((footerlink) => {
//             footerlink.style.color = '#ffffff';
//             footerlink.onmouseover = function (event) {
//                 event.target.style.color = '#4bcaff';
//             }
//             footerlink.onmouseout = function (event) {
//                 event.target.style.color = '#ffffff';
//             }
//         })
//     } else {
//         bodyBgr.style.backgroundColor = '';
//         logoColor.style.color = '';
//         sectionTextColor.forEach((text) => {
//             text.style.color = '';
//         });
//         mainMenuLinkColor.forEach((link) => {
//             link.style.color = '';
//             link.onmouseover = function (event) {
//                 event.target.style.color = '';
//             }
//             link.onmouseout = function (event) {
//                 event.target.style.color = '';
//             }
//         });
//         footerMenuLinkColor.forEach((footerlink) => {
//             footerlink.style.color = '';
//             footerlink.onmouseover = function (event) {
//                 event.target.style.color = '';
//             }
//             footerlink.onmouseout = function (event) {
//                 event.target.style.color = '';
//             }
//         })
//     }
//   }

// updateStyles();

// pushBtn.onclick = function () {
//   styleChanged = !styleChanged;
//   localStorage.setItem('styleChanged', styleChanged);
//   updateStyles();
// }


// Варіант 2

const pushBtn = document.querySelector('.theme-button'); 
let styleChanged = true; 
const styleId = document.getElementById('styleid');
console.log(styleId.href);

const LSStyleChanged = localStorage.getItem('styleChanged');
if (LSStyleChanged !== null) {
  styleChanged = LSStyleChanged === 'true'; 
}

function updateStyles() {
    if (styleChanged) {
      styleId.href = 'http://127.0.0.1:5500/css/main-theme.css';
    }
    else {
        styleId.href = 'http://127.0.0.1:5500/css/main.css';
            }
        }

updateStyles();

pushBtn.onclick = function () {
  styleChanged = !styleChanged;
  localStorage.setItem('styleChanged', styleChanged);
  updateStyles();
}


